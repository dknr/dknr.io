var gulp = require('gulp');
var pug = require('gulp-pug');
var sass = require('gulp-sass');

gulp.task('html', function () {
    return gulp.src('src/**/*.pug')
        .pipe(pug())
        .pipe(gulp.dest('src'))
        .pipe(gulp.dest('out'));
});

gulp.task('css', function () {
    return gulp.src('src/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('src'))
        .pipe(gulp.dest('out'));
});

gulp.task('pix', function () {
    return gulp.src('src/pix/**/*.jpg')
        .pipe(gulp.dest('out/pix'));
});

gulp.task('default', ['html', 'css', 'pix']);
